
$(function(){
  //mobile menu
  $('body').on('click', '.fa-bars', function(e) {
    e.stopPropagation();
    $('#nav').stop().toggleClass('open');
    $('body').stop().toggleClass('modal-open');
  });    
  //scroll to div
  $(function() {
    $('a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });
  });  

});

//Youtube Lazy Loader
document.addEventListener('DOMContentLoaded',
    function() { 
      convertYoutubeEmbeds();
    }); 

window.addEventListener('load', function(){
    var c = getParameterByName("Contact");
    if(c !== null && c !== ""){
      setTimeout(function(){
        //smartlook('tag', 'email', c);
      }, 500);
    }
});

function buildYoutubeThumb(id) {
    var thumb = '<img src=\"https://i.ytimg.com/vi/ID/maxresdefault.jpg\">',
        play = '<div class=\"play\"></div>';
    return thumb.replace('ID', id) + play;
}

function buildYoutubeIframe() {
    var s;
    s = document.createElement('script');
    s.src = "https://www.youtube.com/iframe_api";
    document.head.appendChild(s);

    var iframe = document.createElement('iframe');
    var embed = 'https://www.youtube.com/embed/ID?enablejsapi=1&autoplay=1&rel=0';
    iframe.setAttribute('src', embed.replace('ID', this.dataset.id));
    iframe.setAttribute('frameborder', '0');
    iframe.setAttribute('allowfullscreen', '1');
    this.parentNode.replaceChild(iframe, this);
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function convertYoutubeEmbeds(){
        var div, n,
            v = document.getElementsByClassName('youtube-player');
        for (n = 0; n < v.length; n++) {
            div = document.createElement('div');
            div.setAttribute('data-id', v[n].dataset.id);
            div.innerHTML = buildYoutubeThumb(v[n].dataset.id);
            div.onclick = buildYoutubeIframe;
            v[n].appendChild(div);
        }

}